bucket_name="guest-book"

front_version=$1
backend_url="http://158.160.143.231"

# create temporary directory to add file with version
cp -r ./front ./tmp

echo $backend_url

sed -i "s#XXXX#$backend_url#g" ./tmp/index.html
sed -i "s/YYYY/$front_version/g" ./tmp/index.html

s3cmd put ./tmp/index.html s3://$bucket_name

rm -rf ./tmp