import os
import uuid
import uvicorn
from fastapi import FastAPI, Body, Response
from fastapi.middleware.cors import CORSMiddleware

from repository import comments_repository

BACKEND_ID = f"XXXX#{uuid.uuid4()}"

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get(
    "/ping",
)
def get_comments():
    return {
        "backend": BACKEND_ID,
    }

@app.get(
    "/comments",
)
def get_comments(limit: int = 10):
    return {
        "comments": comments_repository.select(limit=limit),
        "backend": BACKEND_ID,
    }


@app.post(
    "/comments/new",
)
def create_new_comment(response: Response, identifier: str = Body(), comment: str = Body()):
    print(identifier)
    print(comment)
    comments_repository.create_comment(identifier=identifier, comment=comment )
    response.status_code = 201
    return {
        "backend": BACKEND_ID,
    }


if __name__ == "__main__":
    print(os.getenv("APP_HOST"))
    print(os.getenv("YDB_ENDPOINT"))
    print(BACKEND_ID)
    uvicorn.run(
        app,
        host=os.getenv("APP_HOST", "localhost"),
        port=int(os.getenv("APP_PORT", "80")),
        log_level="debug",
    )
