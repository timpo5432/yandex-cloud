import datetime
import os
import uuid

import ydb

from dbClient import ydb_client


class CommentsRepository:

    def select(self, limit: int = 10) -> list[dict]:
        def callee(session: ydb.Session):
            return session.transaction().execute(
                """
                SELECT *
                FROM `{}`
                 """.format("comments"),
                commit_tx=True,
                settings=ydb.BaseRequestSettings().with_timeout(3).with_operation_timeout(2)
            )[0].rows

        return [dict(row) for row in ydb_client.session_pool.retry_operation_sync(callee)]

    def create_comment(self, identifier: str, comment: str) -> None:
        def callee(session: ydb.Session):
            session.transaction().execute(
                """
                INSERT INTO `{}` (id, identifier, comment)
                VALUES ("{}", "{}", "{}")
                """.format(
                    "comments",
                    uuid.uuid4(),
                    identifier,
                    comment
                ),
                commit_tx=True,
                settings=ydb.BaseRequestSettings().with_timeout(3).with_operation_timeout(2)
            )

        ydb_client.session_pool.retry_operation_sync(callee)


comments_repository = CommentsRepository()
