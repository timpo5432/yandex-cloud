#!/bin/bash

instance=$1
tag=$2
registry_id="crpd3uohb3gl16id876f"

yc compute instance update-container \
--id $instance \
--container-image=cr.yandex/$registry_id/comments-service:$tag \
--container-command=/app/start.sh \
--container-env-file=./back/.env \
