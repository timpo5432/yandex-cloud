# yandex-cloud

надо настроить yc и s3

`yc config set token <oauth-token>

yc config set cloud-id b1gnuuuvdi0shkm9h2o4

yc config set folder-id b1guuhslc80h72btrgme

yc container registry configure-docker

s3cmd --configure
`

настройка s3 https://cloud.yandex.ru/ru/docs/storage/tools/s3cmd

нужно положить ключ ydb в ./back/ydb_key: https://console.cloud.yandex.ru/folders/b1guuhslc80h72btrgme/service-account/ajef0d3q846pq676t5qu


ссылка на сайт - http://guest-book.website.yandexcloud.net/
вверху пишется версия бэкенда, так как оно где-то там кешируется, то меняется при обновлении раз в минуту примерно


обновить фронт:

`./update/updateFront.sh <front_version>`

собрать бэк:

`./update/buildDockerBackend.sh <tag>`

создать новый инстанс вм:

` ./update/deployBack.sh <tag> <public_ssh>`

обновить бэк:
`./update/updateBack.sh <instance_id> <tag>`