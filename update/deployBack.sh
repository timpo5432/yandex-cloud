tag=$1
pubSsh=$2
registry_id=crpd3uohb3gl16id876f
target_group_id=enpnpqddal1a2u5qanit

replica_id=$(yc compute instance create-with-container \
--zone ru-central1-a \
--ssh-key $pubSsh \
--platform standard-v3 \
--create-boot-disk size=30 \
--public-ip \
--container-name=backend \
--container-image=cr.yandex/$registry_id/comments-service:$tag \
--container-command=/app/start.sh \
--container-env-file=./back/.env \
--container-privileged | grep "^id: " | awk '{split($0,a,": "); print a[2]}')

replica_address=$(yc compute instance list | grep $replica_id | awk '{split($0,a,"|"); gsub(/ /, "", a[7]); print a[7]}')

yc load-balancer target-group add-targets --id $target_group_id --target subnet-name=default-ru-central1-a,address=$replica_address
yc load-balancer target-group add-targets --id b1guuhslc80h72btrgme --target subnet-name=default-ru-central1-a,address=84.201.172.28

