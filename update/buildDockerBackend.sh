#!/bin/bash

registry_id=crpd3uohb3gl16id876f
tag=$1

# create temporary directory to add file with version
cp -r ./back ./tmp2

sed -i "s/XXXX/$tag/g" ./tmp2/main.py

# build docker image
docker build ./tmp2 -t cr.yandex/$registry_id/comments-service:$tag

# push docker image
docker push cr.yandex/$registry_id/comments-service:$tag

# remove temporary directory
rm -rf ./tmp2
